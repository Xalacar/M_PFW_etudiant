{-# LANGUAGE OverloadedStrings #-}

-- import Control.Concurrent (forkIO)
-- import Control.Monad (forever)
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (fromStrict)
-- import Data.IORef (atomicWriteIORef, IORef, newIORef, readIORef)
import qualified Web.Scotty as SC

import OpenCV
import OpenCV.VideoIO.Types

main :: IO ()
main = do
    capMaybe <- openCam
    case capMaybe of
        Nothing -> putStrLn "couldn't open device"
        Just cap -> runServer 3042 cap

runServer :: Int -> VideoCapture -> IO ()
runServer port cap = SC.scotty port $ do
    SC.get "/" $ SC.file "index-http.html"
    SC.get "/out.png" $ do
        SC.setHeader "Content-Type" "image/png"
        Just png <- SC.liftAndCatchIO $ captureCam cap
        SC.raw $ fromStrict png

openCam :: IO (Maybe VideoCapture)
openCam = do
    cap <- newVideoCapture
    exceptErrorIO $ videoCaptureOpen cap $ VideoDeviceSource 0 Nothing
    isOpened <- videoCaptureIsOpened cap
    case isOpened of
        False -> return Nothing
        True -> do
            _ <- videoCaptureSetD cap VideoCapPropFps 5
            return $ Just cap

captureCam :: VideoCapture -> IO (Maybe ByteString)
captureCam cap = do
    _ <- videoCaptureGrab cap 
    imgMaybe <- videoCaptureRetrieve cap 
    case imgMaybe of
        Nothing -> return Nothing
        Just img -> return $ Just $ exceptError $ imencode params img
    where params = OutputPng defaultPngParams

