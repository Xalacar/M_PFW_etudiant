{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") { } }:

let

  _miso = pkgs.haskell.packages.ghcjs.callCabal2nix "miso" (pkgs.fetchFromGitHub {
    rev = "0.21.1.0";
    sha256 = "19x9ym4399i6ygs0hs9clgrvni0vijfg4ff3jfxgfqgjihbn0w4r";
    owner = "haskell-miso";
    repo = "miso";
  }) {};

  bmxriders_client_miso = pkgs.haskell.packages.ghcjs.callCabal2nix "bmxriders_client_miso" ./. {
    miso = _miso; 
  };

  inherit (pkgs) closurecompiler;

  bmxriders_client_miso_pkg = pkgs.runCommand "bmxriders_client_miso_pkg" { inherit bmxriders_client_miso; } ''
    mkdir -p $out
    ls ${bmxriders_client_miso.src}
    ls ${bmxriders_client_miso}
    cp -R ${bmxriders_client_miso.src}/static $out/
    cp ${bmxriders_client_miso.src}/index.html $out/
    cd ${bmxriders_client_miso}/bin/bmxriders-client-miso.jsexe
    ${closurecompiler}/bin/closure-compiler all.js > $out/all.js
  '';

in 

if pkgs.lib.inNixShell then bmxriders_client_miso.env else bmxriders_client_miso_pkg

