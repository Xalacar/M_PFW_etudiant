{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") { } }:

let

  pkg = pkgs.stdenv.mkDerivation {
    name = "bmxriders_client_elm_pkg";
    src = ./.;
    buildInputs = [ pkgs.elmPackages.elm-make ];
    buildPhase = ''
      export HOME=`pwd`
      elm-make Main.elm --yes --output all.js
      ${pkgs.closurecompiler}/bin/closure-compiler all.js --compilation_level=ADVANCED_OPTIMIZATIONS --jscomp_off=checkVars > all.min.js

    '';
    installPhase = ''
      mkdir -p $out
      cp -R static $out/
      cp index.html all.min.js $out/
    '';
  };

  env = with pkgs; stdenv.mkDerivation {
    name = "bmxriders_client_elm_env";
    src = ./.;
    buildInputs = with elmPackages; [
      elm-format
      elm-make
      elm-reactor
      elm-repl
    ];
    installPhase = "";
    shellHook = ''
      alias mymake="elm-make Main.elm --yes --output all.min.js"
    '';
  };

in

if pkgs.lib.inNixShell then env else pkg

