module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (height, href, src, width)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as JD


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { modelText : String }


init : ( Model, Cmd Msg )
init =
    ( Model "", Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type Msg
    = SetModelText String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetModelText str ->
            ( Model str, Cmd.none )


view model =
    span []
        [ h1 [] [ text "Bmx riders (Elm)" ]
        , p []
            [ input [ onInput SetModelText ] [] ]
        , p []
            [ text model.modelText ]
        ]
