{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson ((.:), eitherDecode, FromJSON, parseJSON, withObject)
import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Network.HTTP.Conduit (simpleHttp)
import System.Environment (getArgs)

data Film = Film
    { titre :: Text
    , annee :: Int
    , realisateur :: Text
    } deriving (Show, Generic)

main :: IO ()
main = do
    args <- unwords <$> getArgs
    print args

