{-# LANGUAGE OverloadedStrings #-}
import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid
import qualified Web.Scotty as S
import Network.Wai.Middleware.RequestLogger (logStdoutDev)

divCss = C.div C.# C.byClass "divCss" C.? do
  C.backgroundColor  C.beige
  C.border           C.solid (C.px 1) C.black

main = S.scotty 3000 $ do
  S.middleware logStdoutDev

  S.get "/" $ S.html $ renderText $ html_ $ do
    body_ $ do
      h1_ "The helloscotty project !"
      a_ [href_ "/hello"] ("Go to the hello page")

  S.get "/hello" $ do
    name <- S.param "name" `S.rescue` (\_ -> return ("unknown user"::L.Text))
    S.html $ renderText $ html_ $ body_ $ do
      h1_ $ toHtml $ L.concat["Hello ",name]
      form_ [action_ "/hello", method_ "get"] $ do
        input_ [name_ "name", value_ "enter name here"]
        input_ [type_ "submit"]
      a_ [href_ "/"] ("Go to the home page")
