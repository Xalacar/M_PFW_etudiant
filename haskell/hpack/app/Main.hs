{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs)

main :: IO ()
main = do
	let TIO.myArgs = getArgs 
	let T.t = head myArgs
	f <- readFile t
	(mapM_ putStrLn) . lines $ T.f

