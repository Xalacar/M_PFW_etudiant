{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.open "music.db"

    putStrLn "\n*** id et nom des artistes ***"
    resArtist <- SQL.query_ conn "SELECT * FROM artists" :: IO [(Int,T.Text)]
    print resArtist

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    resRadioHead <- SQL.query_ conn "SELECT * FROM artists WHERE name='Radiohead'" :: IO [(Int, T.Text)]
    print resRadioHead
	
    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    res3 <- SQL.query conn "SELECT * FROM titles WHERE id>? AND lower(name) LIKE ?" (1::Int, "%ust%"::T.Text) :: IO [(Int, Int, T.Text)]
    print res3

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    resName <- SQL.query_ conn "SELECT name FROM titles":: IO [(Name)]
    print resName

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    resNameText <- SQL.query_ conn "SELECT name FROM titles":: IO [[(T.Text)]]
    print resNameText

    SQL.close conn

