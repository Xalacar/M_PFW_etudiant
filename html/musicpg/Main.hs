{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import Lucid


data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field

data Music = Music
    { _title :: T.Text
    , _artist :: T.Text
    } deriving (Show)

instance FromRow Music where
    fromRow = Music <$> field <*> field
    
getMusic :: SQL.Connection -> IO [Music]
getMusic conn = SQL.query_ conn "SELECT titles.name,artists.name FROM artists INNER JOIN artists.id = titles.artist " :: IO [Music]


myCss :: C.myCss
myCss = C.div C.# C.byClass "myCss" C.? do
    C.backgroundColor C.beige
    C.border          C.solid (C.px 1) C.black


myPage :: [Music] -> Html [()]
myPage titles = do 
    doctype_
    html_ $ do
      head_ $ do
          meta_ [charset_ "utf-8"]
          title_ "My music"
          style $ L.toStrict
      body_ $ do
          resArtist <- 
          --print resArtist
          ul_ $ mapM_ (li_ . toHtml) resArtist

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=mymusic user='toto' password='toto'"
    titles <- getTitles conn
    renderToFile "music_html.html" $ myPage titles
    SQL.close conn
    
    
		
					







